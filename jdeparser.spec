Name:             jdeparser
Version:          2.0.0
Release:          6
Summary:          Source generator library for Java
License:          ASL 2.0
URL:              https://github.com/jdeparser/jdeparser2
Source0:          https://github.com/jdeparser/jdeparser2/archive/%{version}.Final.tar.gz
BuildArch:        noarch

BuildRequires:    graphviz, maven-local, mvn(jdepend:jdepend), mvn(junit:junit), mvn(org.jboss:jboss-parent:pom:), mvn(org.jboss.apiviz:apiviz)
Provides:         %{name}-javadoc%{?_isa} %{name}-javadoc
Obsoletes:        %{name}-javadoc

%description
This project is a fork of Sun's (now Oracle's) com.sun.codemodel project. We
decided to fork the project because by all evidence, the upstream project is
dead and not actively accepting outside contribution. All JBoss projects are
urged to use this project instead for source code generation.

%prep
%setup -q -n jdeparser2-%{version}.Final

%build
%mvn_build

%install
%mvn_install

%files -f .mfiles
%license LICENSE.txt
%{_javadocdir}/%{name}/*

%changelog
* Tue Dec 13 2019 openEuler Buildteam <buildteam@openeuler.org> - 2.0.0-6
- Package init

